"""Remote host configuration."""
from os import getenv, path
from dotenv import load_dotenv
from app_logger import LOG

# Load environment variables from .env
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

# Read environment variables
host = getenv("REMOTE_HOST")
username = getenv("REMOTE_USERNAME")
password = getenv("REMOTE_PASSWORD")
config_values = [
    {"host": host},
    {"user": username},
    {"password": password}
]

for config in config_values:
    if None in config.values():
        LOG.warning(f"Config value not set: {config.popitem()}")
        raise Exception("Set your environment variables via a .env file.")
