"""Custom logger configuration."""
from sys import stdout
from loguru import logger as custom_logger


def create_logger() -> custom_logger:
    """Create custom LOGGER."""
    custom_logger.remove()
    # custom_logger.add("log_file.log", rotation="500 MB", enqueue=True, colorize=True, format='{message}')
    custom_logger.add(stdout,  format='{message}')
    return custom_logger


LOG = create_logger()
