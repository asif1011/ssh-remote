from src.paramiko_client import RemoteClient
from app_logger import LOG
from config import (host, password, username)


def main():
    """Initialize remote host client and execute actions."""
    remote = RemoteClient(host, username, password)
    try:
        remote.login_vsp()
    except Exception as exc:
        LOG.error(f"VSP login failed: {exc}")
        remote.disconnect()

    try:
        remote.execute_commands()
        remote.disconnect()
    except Exception as exc:
        LOG.error(f"Execution of commands failed: {exc}")
        remote.disconnect()
