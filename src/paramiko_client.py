"""Client to handle connections and actions executed against a remote host."""
import os
import pathlib
from paramiko import AutoAddPolicy, SSHClient
from paramiko.ssh_exception import AuthenticationException
from app_logger import LOG


class RemoteClient:
    """Client to interact with a remote host via SSH & SCP."""

    def __init__(self, host: str, user: str, password: str):
        self.host = host
        self.user = user
        self.password = password
        self.client = self.get_client()

    def get_client(self):
        """Open connection to remote host. """
        try:
            client = SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(AutoAddPolicy())
            client.connect(self.host, username=self.user, password=self.password, timeout=5000)
            return client
        except AuthenticationException as exc:
            LOG.error(f"Authentication failed: {exc}")
            raise exc

    def disconnect(self):
        """Close SSH connection."""
        if self.client:
            self.client.close()

    def login_vsp(self):
        channel = self.client.invoke_shell()
        channel.send(b'sudo apt autoremove')
        channel.send(b'\n')
        if channel.recv_ready():
            channel.send(b'myself7860!@#')
            channel.send(b'\n')
            if channel.recv_ready():
                LOG.info(channel.recv(1000000))
        else:
            raise Exception('kuch to gardbard hai')

    def execute_commands(self):
        """
        Execute multiple commands in succession.
        """
        commands = open(os.path.join(pathlib.Path(__file__).parents[1], 'commands.txt'), encoding='utf8',
                        errors='ignore').readlines()
        commands = [cmd.strip('/') for cmd in commands]
        for cmd in commands:
            std_in, stdout, stderr = self.client.exec_command(cmd)
            stdout.channel.recv_exit_status()
            response = stdout.readlines()
            for line in response:
                LOG.info(line)
